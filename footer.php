<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Korenbest
 */

?>

	<footer id="footer">
		<div class="footer footer-1">
			<div class="container">
				<div class="row">

					<?php if ( is_active_sidebar( 'sidebar1') ) : ?>
						<!-- Columns-->
						<div class="column col-md-6 col-lg-3">
							<div class="box">
								<?php dynamic_sidebar('sidebar1'); ?>
							</div><!-- .box -->
						</div><!-- .col -->
					<?php endif; ?>

					<?php if ( is_active_sidebar('sidebar-2') ) :?> 
						<!-- Columns -->
						<div class="column col-md-6 col-lg-3">
							<div class="box">
								<?php dynamic_sidebar('sidebar-2'); ?>
							</div><!-- .box -->
						</div><!-- .col -->
					<?php endif; ?>

					<?php if(get_field('title_nieuwsbrief', 'option')) : ?>
						<!-- Columns -->
						<div class="column col-md-12 col-lg-6">
							<div class="box">
								<h3 class="title"><?php the_field('title_nieuwsbrief', 'option'); ?></h3>
								<div class="content-email">
									<p>
										<?php the_field('content_nieuwsbrief', 'option'); ?>
									</p>
								</div>

								<!-- Form -->
								<div class="form">
									<div class="input">
										<?php
											echo do_shortcode(get_field('form_footer', 'option'));
										?>
									</div>
								</div><!-- .form -->
							</div><!-- .box -->
						</div><!-- .col -->
					<?php endif; ?>
					
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .footer-1 -->

		<div class="footer footer-2">
			<div class="container">
				<?php 
					$image = get_field('logo', 'option');
					if ($image) : ?>
						<a href="<?php echo site_url(); ?>">
							<img class="custom-logo" src="<?php echo $image['url']; ?>">
						</a>
				<?php endif; ?>
				<div class="info">
					<div class="address"><?php the_field('address', 'option'); ?></div>
					-
					<div class="telp">Bel ons: <a href="tel:<?php echo str_replace(' ', '', str_replace('-', '', get_field('contact', 'option'))) ?>"><?php the_field('contact', 'option'); ?></a></div>
					-
					<div class="email">Email: <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a></div>
				</div>
			</div><!-- .container -->
		</div><!-- .footer-2 -->

		<div class="footer footer-3">
			<div class="container">
				Get Social:
				<?php if ( get_field ('facebook', 'option') ): ?>
					<a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fab fa-facebook"></i></a>
				<?php endif; ?>
				<?php if ( get_field ('instagram', 'option') ): ?>
					<a href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fab fa-instagram"></i></a>
				<?php endif; ?>
				<?php if ( get_field ('google', 'option') ): ?>
					<a href="<?php the_field('google', 'option'); ?>" target="_blank"><i class="fab fa-google-plus-square"></i></a>
				<?php endif; ?>
				<?php if ( get_field ('linked in', 'option') ): ?>
					<a href="<?php the_field('linked_in', 'option'); ?>" target="_blank"><i class="fab fa-linkedin"></i></a>
				<?php endif; ?>
			</div><!-- .container -->
		</div><!-- .footer-3 -->

		<div class="footer footer-4">
			<div class="container">
				<div class="left"><i class="far fa-copyright"></i>Copyright <?php echo date("Y"); ?> - <b><a href="<?php echo site_url(); ?>">Koren Best</a></b></div>
				<div class="right">Designed and developed by <b><a href="https://www.wappstars.nl/" target="_blank">Wappstars</a></b></div>
			</div><!-- .container -->
			<div class="container legals-menu">
				<?php
				$args = array(
					'theme_location' => 'legals'
				);
				
				if (has_nav_menu('legals')) {
					wp_nav_menu($args);
				}
				?>
			</div> <!--  .container -->
		</div><!-- .footer-4 -->
	</footer><!-- #footer -->

</body>
</html>
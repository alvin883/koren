<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Korenbest
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function korenbest_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'korenbest_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function korenbest_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'korenbest_pingback_header' );


/**
 * Add Bootstrap button classes to tag cloud
 */
function korenbest_tag_cloud_btn( $return ) {
	$return = str_replace('<a', '<a class="btn btn-secondary btn-sm"', $return );
	return $return;
}
add_filter( 'wp_tag_cloud', 'korenbest_tag_cloud_btn' );


/**
 * Customize the Read More Button
**/
function korenbest_modify_read_more_link() {
    return sprintf( '<a class="more-link btn btn-sm btn-secondary" href="%1$s">%2$s</a>',
        get_permalink(),
        __( 'Read More', 'korenbest' )
    );
}
add_filter( 'the_content_more_link', 'korenbest_modify_read_more_link' );

/*
* Custom menu output
* No <ul>, no <li>, just <a>
**/
function korenbest_menu($location) {
  // Get our nav locations (set in our theme, usually functions.php)
  $menuLocations = get_nav_menu_locations(); // This returns an array of menu locations;

  $menuID = $menuLocations[$location]; // Get the *MENU* menu ID

  $menu_navs = wp_get_nav_menu_items($menuID);

  $queried_page_id = get_queried_object_id();
  ?>
    <ul class='navbar-nav'>
      <?php
      foreach ( $menu_navs as $menu_nav ) {

        $object_id = intval($menu_nav->object_id);

        if ( $queried_page_id == $object_id ) {
          $active = " class='active' ";
        } else {
          $active = '';
        }

        echo '<li class="nav-item"><a href="'. esc_url( $menu_nav->url ) .'" '. $active .' title="'. esc_html( $menu_nav->title ) .'">'. esc_html( $menu_nav->title ) .'</a></li>';
      }
      ?>
    </ul><!-- close UL -->
  <?php 
}

/* The content Limitation*/ 
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) { 
    array_pop($content);
    $content = implode(" ",$content).' ...';
  } else {
    $content = implode(" ",$content);
  } 
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
  $content = strip_tags($content);
  return $content;
}


/* Function ACF */
if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
    'page_title'  => 'Theme General Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Theme Header Settings',
    'menu_title'  => 'General Data',
    'parent_slug' => 'theme-settings',
  )); 

}


/* Custom WP native gallery output*/
function customFormatGallery($string, $attr){

  $output = '<div class="row gallery-native">';
  $posts = get_posts(array('include' => $attr['ids'], 'post_type' => 'attachment'));

  foreach($posts as $imagePost){

      if(empty($attr['size'])){ $attr['size'] = null; }

      $columns = (isset($attr['columns'])) ? $attr['columns'] : 3;
      $output .= "<a class='gallery-columns-" . $columns . "' href='" . wp_get_attachment_image_src($imagePost->ID, 'full')[0] . "' title='" . get_the_title($imagePost->ID) . "'>
      <img src='" . wp_get_attachment_image_src($imagePost->ID, $attr['size'])[0] . "'/>
      </a>";
  }

  $output .= "</div>";

  return $output;
}
add_filter('post_gallery','customFormatGallery',10,2);

/* Post Pagination */
add_filter('next_posts_link_attributes', 'next_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'prev_posts_link_attributes');

function next_posts_link_attributes(){
  return 'class="next page-numbers"';
} 
 
function prev_posts_link_attributes(){
  return 'class="prev page-numbers"';
}



<?php
/**
 * @package Korenbest
 * @subpackage theme name here
 * Template Name: Homepage	
 */
?>
<?php get_header(); ?>

	<?php include get_stylesheet_directory() . '/includes/slider.php'; ?>

	<div class="sequence">
		<div class="layer-bg">
			<div class="item dot dot-1"></div>
			<!-- <div class="item line"></div> -->
			<div class="item dot dot-2"></div>
		</div>

		<?php include get_stylesheet_directory() . '/includes/post-section.php'; ?>
		
		<!-- break section with background -->
		<div class="section-break"></div>
		<!-- END -->

		<?php include get_stylesheet_directory() . '/includes/highlight-menukaart.php'; ?>
	</div>


<?php 
get_footer();
<?php
/**
 * The template for displaying page 'News'
 *
 *
 *
 * Template Name: Blog
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Korenbest
 */

get_header();

if(have_posts()) :
    while(have_posts()) : the_post(); ?>

        <div id="content">
            <div id="list_news">
                <div class="sequence">
                    <div class="layer-bg">
						<div class="item dot dot-1"></div>
						<!-- <div class="item line"></div> -->
						<div class="item dot dot-2"></div>
                    </div>

                    <div class="section header" <?php if( has_post_thumbnail() ){
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>>
                        <div class="container">
                            <div class="card green">
                                <div class="card-body">
                                    <h1 class="card-title"> <?php the_title(); ?> </h1>
                                </div><!-- .card-body -->
                            </div><!-- .card -->
                        </div><!-- .container -->
                    </div><!-- .section -->

                    <div class="section content">
                        <div class="container">
                                <?php
                                $ourCurrentPage = get_query_var('paged');
                                $args = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => 6,
                                    'paged' => $ourCurrentPage
                                );
                                $wp_query = new WP_Query($args); 

                                if ( $wp_query->have_posts()) : ?> 
                                
                                    <!-- Start List of cards -->
                                    <div class="row" id="list_news-row">
                                        <?php
                                            while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                                                <div class="column col-md-6 col-lg-4">
                                                    <div class="card bordered">
                                                        <?php if(has_post_thumbnail()) { ?>
                                                            <a href="<?php the_permalink(); ?>">
                                                            <div class="card-img-top" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"></div></a>
                                                        <?php } ?>
                                                        <div class="card-body have-action">
                                                            <h3 class="card-title"><?php the_title(); ?></h3>
                                                            <div class="card-title-data">
                                                                Posted by <?php the_author(); ?> | 
                                                                <?php the_time('F j, Y'); ?>
                                                            </div>
                                                            <div class="card-text">
                                                                <?php echo content(20); ?>
                                                            </div>
                                                            <a href="<?php the_permalink(); ?>" class="btn">
                                                                Lees meer 
                                                                <div><i class="fas fa-chevron-right"></i></div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php endwhile; ?>
                                    </div>
                                    <!-- Bigger than 500 px screen -->
                                    <div class="post-navigation wide">
                                        <?php 
                                            echo paginate_links(array(
                                                'total' => $wp_query->max_num_pages
                                            ));
                                        ?>
                                    </div>
                                    <!-- Smaller than 500 px screen -->
                                    <div class="post-navigation small">
                                        <?php
                                            previous_posts_link('&laquo; Previous');
                                            next_posts_link('Next &raquo;'); 
                                        ?>
                                    </div>
                                     
                                    <?php 
                                       wp_reset_query();
                                    ?>
                                <?php else: ?>
                                    <p><?php _e( 'Sorry, there is no post yet' ); ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div><!-- .section-content -->

                </div><!-- .sequence -->
            </div><!-- #overons -->
        </div><!-- #content-->

    <?php endwhile; ?>
<?php else:
    echo "Sorry, no post were found";
endif; ?>

<?php get_footer();

<?php
/**
 * Template part for displaying `Contact` page
 * 
 * Template Name: Contact Page
 * 
 */
?>

<?php get_header();

if(have_posts()) :
    while(have_posts()) : the_post(); ?>

		<div id="content">
			<div id="contact">

				<div class="sequence">
					<div class="layer-bg">
						<div class="item dot dot-1"></div>
						<!-- <div class="item line"></div> -->
						<div class="item dot dot-2"></div>
					</div><!-- .layer-bg -->

					<div class="header">
						<div class="container">
                            <div class="card green">
                                <div class="card-body">
                                    <h1 class="card-title"> <?php the_title(); ?> </h1>
                                    <div class="card-text" id="content-wp">
                                        <?php the_content(); ?>
                                    </div>
                                </div><!-- .card-body -->
                            </div><!-- .card green -->
						</div><!-- .container -->
					</div><!-- .header -->

                    <div class="section maps-2">
                        <div class="container">
                            <?php if(get_field('title_gmaps')) : ?>
                                <div class="title_wrapper">
                                    <h2 class="block title">
                                        <?php the_field('title_gmaps'); ?>
                                    </h2>
                                </div>
                            <?php endif; ?>
                                <?php if(get_field('map_tag') ) :
                                    echo get_field('map_tag');
                                endif; ?>
                        </div><!-- .container -->
                    </div><!-- .maps -->

					<div class="section embed-form style-2">
                        <div class="container">
                            
                            <div class="card green">
                                <div class="card-body">
                                    <h2 class="card-title"><?php the_field('title_form_section'); ?></h2>
                                    <div class="card-text">
                                        <?php the_field('content_form_section'); ?>
                                    </div>
                                </div><!-- .card-body -->
                            </div><!-- .card green -->

                            <!-- Form -->
                            <div id="form_reserveren" class="form">
                                <?php  
                                    echo do_shortcode(get_field('form_contact', 'option'));
                                ?>
                            </div><!-- .form -->

                        </div><!-- .container -->
                    </div><!-- .section -->

                </div><!-- .sequence -->

			</div><!-- #impressie -->
		</div><!-- #content -->

    <?php endwhile;
else :
    echo "sorry, no post were found";
endif;

get_footer(); ?>
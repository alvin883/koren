<?php
/**
 * Template part for displaying page `Menukaart`
 * 
 * Template Name: Menukaart Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Korenbest
 */
?>
<?php get_header();

if(have_posts()) :
    while(have_posts()) : the_post(); ?>

    <div id="content">
        <div id="menukaart">
            <div class="sequence">
                <div class="layer-bg">
                    <div class="item dot dot-1"></div>
                    <!-- <div class="item line"></div> -->
                    <div class="item dot dot-2"></div>
                </div>
                <div class="section section-3 header">
                    <div class="background" style="background-image: url('<?php if (has_post_thumbnail()){ the_post_thumbnail_url(); } ?>')">
                    </div><!-- side background -->
                
                    <div class="container">
                        <?php 
                            $file = get_field('menukaart_file');
                            $title = get_field('menukaart_title');

                        ?>
                            <div class="card green">
                                <div class="card-body">
                                    <h1 class="card-title"> <?php the_title(); ?></h1>
                                    <div class="card-text" id="content-wp">
                                        <?php echo the_content(); ?>
                                    </div>
                                    <div class="action">
                                        <a href="<?php  
                                            $file = get_field('menukaart_file');
                                            if( $file ): 
                                                echo $file['url'];
                                            endif;
                                            ?>" class="btn download-menukaart-btn">
                                                Download Menukaart
                                            <i class="fas fa-arrow-down"></i>
                                        </a>
                                    </div>
                                </div><!-- .card-body -->
                            </div><!-- .card -->
                    </div><!-- .container -->
                </div><!-- .section -->

            </div><!-- .sequence -->
        </div><!-- #overons -->
    </div><!-- #content-->

    <?php endwhile; 
else:
    echo "Sorry, no post were found";
endif;

get_footer(); ?>
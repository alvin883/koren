<?php
/**
 * Template part for displaying page `Over Ons`
 * 
 * Template Name: Overons & Restaurant Sub Menu Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Korenbest
 */
?>
<?php get_header();

if(have_posts()) :
    while(have_posts()) : the_post(); ?>

    <div id="content">
        <div id="overons">
            <div class="sequence">
                <div class="layer-bg">
                    <div class="item dot dot-1"></div>
                    <!-- <div class="item line"></div> -->
                    <div class="item dot dot-2"></div>
                </div>
                <div class="section header">
                    <div class="bg parallax" <?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>></div><!-- .bg -->
                    <div class="container">
                        <div class="card green">
                            <div class="card-body">
                                <h1 class="card-title"> <?php the_title(); ?> </h1>
                                <div class="card-text" id="content-wp">
                                    <?php the_content(); ?>
                                </div>
                            </div><!-- .card-body -->
                        </div><!-- .card -->
                    </div><!-- .container -->
                </div><!-- .section -->

                <?php if(get_field('description_title') || have_rows('koren_list')) : ?>        
                    <div class="section list list-1">
                        <div class="container">
                            <?php if(get_field('description_title')) : ?>
                                <h2 class="title"><?php the_field('description_title'); ?></h2>
                            <?php endif; ?>
                            <div class="row">
                                <?php if(have_rows('koren_list')) :
                                    while(have_rows('koren_list')) : the_row(); ?>
                                        <div class="column col-md-6 col-sm-12">
                                            <div class="content"><?php the_sub_field('list_point'); ?></div>
                                      </div>
                                     <?php endwhile;
                                endif; ?>
                                
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                
                <?php if(get_field('title_child_page_list') || have_rows('child_page_list')) : ?>
                    <div class="section list list-2">
                        <div class="container">
                            <?php if(get_field('title_child_page_list')) : ?>
                                <h2 class="title"><?php the_field('title_child_page_list'); ?></h2>
                            <?php endif; ?>

                            <div class="row">
                                <?php
                                    if ( have_rows('child_page_list') ) :
                                        while(have_rows('child_page_list')) : the_row();

                                            $title = get_sub_field('child_page_title');
                                            $url = get_sub_field('child_page_link');
                                            $image = get_sub_field('child_page_image');
                                ?>  
                                            <div class="column col-md-4 col-sm-6">
                                                <a href="<?php echo $url; ?>" class="btn" style="background-image: url('<?php echo $image['url'];?>');">
                                                    <div class="content">
                                                        <span><?php echo $title; ?></span>
                                                    </div><!-- .content -->
                                                </a><!-- .btn -->
                                            </div><!-- .column -->
                                        <?php endwhile;
                                    endif; ?>
                            </div><!-- .row -->
                        </div><!-- .container -->
                    </div><!-- .list -->
                <?php endif; ?>

                <?php if (have_rows('selling_points') || get_field('title_selling_point')) :  ?> 
                    <div class="section list list-3">
                        <div class="container">
                            <?php if(get_field('title_selling_point')) : ?>       
                                <h2 class="title"><?php the_field('title_selling_point'); ?></h2>
                            <?php endif; ?>      
                            <div class="row">
                                <?php while (have_rows('selling_points')) : the_row(); ?>

                                    <div class="column col-lg-4 col-md-6">
                                        <div class="card bordered">
                                            <div class="card-body">
                                                <h3 class="card-title"> <?php echo get_sub_field('selling_point_title'); ?> </h3>
                                                <p class="card-text">
                                                    <?php echo get_sub_field('selling_point_content'); ?>
                                                </p>
                                            </div><!-- .card-body -->
                                        </div><!-- .card -->
                                    </div><!-- .column -->

                                <?php endwhile; ?>
                            </div><!-- .row -->
                        </div><!-- .container -->
                    </div><!-- .list-3 -->
                <?php endif; ?>
                
                <?php if ( get_field('overons_button')) : ?>
                    <a href="<?php echo the_field('link_refers_to'); ?>" class="btn button-bottom big-size"><?php the_field('overons_button'); ?></a>
                <?php endif; ?>                       
            </div><!-- .sequence -->
        </div><!-- #overons -->
    </div><!-- #content-->

    <?php endwhile; 
else:
    echo "Sorry, no post were found";
endif;

get_footer(); ?>
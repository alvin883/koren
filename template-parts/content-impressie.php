<?php
/**
 * Template part for displaying page `Impressie`
 * 
 * Template Name: Impressie Page
 * 
 */

?>
<?php get_header(); 

if (have_posts()) :
	while (have_posts()) : the_post(); ?>

		<div id="content">
			<div id="impressie">

				<div class="sequence">
					<div class="layer-bg">
						<div class="item dot dot-1"></div>
						<!-- <div class="item line"></div> -->
						<div class="item dot dot-2"></div>
					</div>

					<?php
						// Add "enablejsapi" to video iframe
						$video = get_field('video_embed_tag');
						$video = explode("oembed",$video);
						$video = $video[0] . "oembed&enablejsapi=1" . $video[1];
					?>

					<?php if(get_field('video_embed_tag')) : ?>
					<div class="fixed-video">
						<div class="container">
							<?php echo $video; ?>
							<button class="btn toggle-video">
								Close
								<div><i class="far fa-times-circle"></i></div>
							</button>
						</div>
					</div>
					<?php endif; ?>

					<div class="header">
						<div class="container">
							<div class="card green">
								<div class="card-body">
									<h1 class="card-title"> <?php the_title(); ?> </h1>
									<div class="card-text">
										<?php echo get_field('impressie_introduction'); ?>
									</div>
									

									<?php if(get_field('video_embed_tag')) : ?>
									<a class="btn toggle-video">
										Bedrijfs video
										<div><i class="fas fa-chevron-right"></i></div>
									</a>
									<?php endif; ?>

									
								</div><!-- .card-body -->
							</div><!-- .card -->
						</div><!-- .container -->
					</div><!-- .header -->

					<div class="section content" id="content-wp">
						<?php the_content(); ?>
					</div><!-- .section -->
				</div><!-- .sequence -->

			</div><!-- #impressie -->
		</div><!-- #content -->

	<?php endwhile;
else:
	echo "sorry no post were found";
endif; 

get_footer(); ?>
<?php
/**
 * Template part for displaying page 'Reserveren'
 * 
 * Template Name: Reserveren Page
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Korenbest
 */

?>

<?php get_header();

if(have_posts()) :
    while(have_posts()) :the_post(); ?>

        <div id="content">
            <div id="reserveren">
                <div class="sequence">
                    <div class="layer-bg">
                        <div class="item dot dot-1"></div>
                        <!-- <div class="item line"></div> -->
                        <div class="item dot dot-2"></div>
                    </div>
                    <div class="section header">
                        <div class="bg" <?php if( has_post_thumbnail() ){ 
                                echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                            } ?>></div><!-- .bg -->
                    </div><!-- .section -->

                    <div class="section embed-form style-2">
                        <div class="container">

                            <div id="form_reserveren" class="form">
                                
                                <div class="container">
                                    <div class="card green">
                                        <div class="card-body">
                                            <h1 class="card-title"><?php the_field('title_reserveren'); ?></h1>
                                            <div class="card-text">
                                                <?php the_field('content_reserveren'); ?>
                                            </div>
                                        </div><!-- .card-body -->
                                    </div><!-- .card green -->
                                </div><!-- .container -->
                                <?php  echo do_shortcode(get_field('form_reserveren', 'option'));?>
                            </div><!-- .form -->

                        </div><!-- .container -->
                    </div><!-- .section -->

                </div><!-- .sequence -->
            </div><!-- #reserveren -->
        </div><!-- #content-->

    <?php endwhile; 
else: 
    echo "no post were found";
endif;

get_footer(); ?>
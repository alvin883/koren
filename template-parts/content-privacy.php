<?php
/**
 * The template for displaying page 'Privacy Policy'
 *
 *
 *
 * Template Name: Privacy Policy Page
 * 
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Korenbest
 */

get_header();

if(have_posts()) :
    while(have_posts()) : the_post(); ?>

        <div id="content">
            <div id="privacy-policy">
                <div class="sequence">
                    <div class="layer-bg">
                        <div class="item dot dot-1"></div>
                        <div class="item dot dot-2"></div>
                    </div>

                    <div class="section">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-10 col-md-12 mx-auto">
                                    <div class="card">
                                        <div class="card-body">
                                            <h1 class="card-title"><?php the_title(); ?></h1>
                                            <h6 class="card-subtitle mb-2 text-muted">
                                                Laatst bijgewerkt: <?php the_modified_date(); ?>
                                            </h6>
                                            <div class="card-text" id="content-wp">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--.sequence-->
            </div><!--.list_news-->
        </div>
                    

    <?php endwhile; ?>
<?php else:
    echo "Sorry, no post were found";
endif; ?>

<?php get_footer();

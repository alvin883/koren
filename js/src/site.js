jQuery(document).ready(function($){

    parallax();
    
    // Navbar, Toggle small navigation when user scroll
    $(window).scroll(function(){
        var scroll = $(window).scrollTop();
        if(scroll > 100){
            $("#navbar").addClass("small");
        }else{
            $("#navbar").removeClass("small");
        }
    });

    /** Homepage Slider */
    $('.slider').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 8000,
        mobileFirst: true,
    });

    /** Swipebox */
    $( '.gallery-native,.wp-block-gallery' ).each(function(i){
        var newClass= 'gallery_'+ i;
        $(this).addClass(newClass);
        $('.' + newClass + ' a').swipebox();
    });

    /** Mansory Layout *
    CONSIDER REMOVING THIS ONE !!!
    IF THIS UNUSED => REMOVE ISOTOPE-LAYOUT DEPENDENCIES !!!
    if($("#list_news-row").length + "a" == 2){
        $("#list_news-row").isotope({
            itemSelector: '.column',
            masonry: {
                columnWidth: '.column',
                horizontalOrder: true,
                gutter: 0
            }
        })
    }
    IF THIS UNUSED => REMOVE ISOTOPE-LAYOUT DEPENDENCIES !!!
    CONSIDER REMOVING THIS ONE !!!
    */

    /** JS Socials */
    if($("#jssocials").length){
        $("#jssocials").jsSocials({
            shares: [
                {
                    share: 'twitter',
                    logo: 'fab fa-twitter'
                },
                {
                    share: 'facebook',
                    logo: 'fab fa-facebook'
                },
                {
                    share: 'googleplus',
                    logo: 'fab fa-google-plus-g',
                    label: ' +1'
                },
                {
                    share: 'linkedin',
                    logo: 'fab fa-linkedin'
                }
            ]
        });
    }

    /** Checking input field */
    $( 'input' ).focusout(function(){
        if( $(this).val().length > 0 ) return $(this).addClass("active");
        $(this).removeClass("active");
    });

    /** Parallax */
    function parallax(){
        $(window).scroll(function(){
            var scrollW = $(window).scrollTop(),
                parallax = scrollW * .4;
            $(".sequence .layer-bg").css({"top": ( parallax ) });

            // Over Ons Page
            $(".bg.parallax").css({"transform":"translateY("+ parallax +"px)"});

            // Single Page
            $(".image-parallax").css({"background-position-y": "calc( 50% + " + parallax + "px )" });
        });
    }

    /** Scroll to next section */
    $(".next-section").click(function(){
        $("html, body").animate({ scrollTop: $(".section-2").offset().top - 50}, 1000);
    });
    
    /** Fixed Video (Impressie Page) */
    $(".toggle-video").click(function(){
        // Stop youtube iFrame on close
        if($(".fixed-video").hasClass("show")){
            $('.fixed-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        }
        // Toggle `show` class
        $(".fixed-video").toggleClass("show");
    });

    /** Embed Form */
        function inputSuccess(el){$(el).addClass("success").removeClass("error");}
        function inputError(el){$(el).addClass("error").removeClass("success");}

        // Text Field validation on blur
        $(".embed-form input[type=\"text\"],textarea").blur(function(){
            if($(this).val().length > 0) return inputSuccess(this);
            inputError(this);
        });

        // Email validation on blur
        $(".embed-form input[type=\"email\"]").blur(function(){
            var validation = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
            ,email = validation.test(String($(this).val()).toLowerCase());
            if( email ) return inputSuccess(this);
            inputError(this);
        });

        // Set Minimal date selection
        var thisDate = new Date();
        if((thisDate.getMonth() + 1) <= 9) {
            $(".embed-form input[type=\"date\"]").attr('min', thisDate.getFullYear() + "-0" + (thisDate.getMonth()+1) + "-" + thisDate.getDate());
        } else {
            $(".embed-form input[type=\"date\"]").attr('min', thisDate.getFullYear() + "-" + (thisDate.getMonth()+1) + "-" + thisDate.getDate());
        }

        // Disable the placeholder
        $(".embed-form select").each(function(){
            $($(this).find("option")[0]).attr("disabled",true);
        });

        $(".embed-form select").blur(function(){
            if($(this).prop('selectedIndex') > 0) return inputSuccess(this);
            return inputError(this);
        });
    /** END : Embed Form */
});
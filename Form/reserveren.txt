<label>Your Data</label>
[text* naam placeholder"Naam"]
[email* email placeholder"Email"]
[tel* telefoon placeholder"Telefoon"]
<label>Reservation</label>
[date* datum placeholder"Datum"]
[text* tijd placeholder"Tijd"]
[select* reserverenVoor "Reserveren Voor" "Lunch" "High Wine" "High Tea" "High Tea deluxe" "High Tea kids" "Gewoon wat te drinken" "Dineren"]
[select* aantalPersonen "Aantal Personen" "2" "3" "4" "5" "6" "7" "8" "9" "Meer"]
<label>Opmerking</label>
[textarea* opmerking placeholder"Write here ..."]
[submit class:btn "Verzenden"]
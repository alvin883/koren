Korenbest Website Template
===


How to use :
- Install Plugin call "Contact 7 Form" and configurating with valid email in WP Dashboard
- Make forms for reserveren and contact , the default are available at directory "./contact-7-form/"
- Instal CF7 Export CSV then activate the form oujust made, to see the feedback form users.
- Install Plugin call "Mailchimp for WP" in WP Dashboard
- Install Plugin call "ACF (Advanced Custom Field)" in WP Dashboard
- Make a page in wordpress and select the Template Page that you want

##ACF :
1. Homepage :
    Contain all the necessary data in the Landing Page
2. Page Setting :
    * Impressie Page : 
        Contain field for customize "Introduction" section in top of page Impressie, If you want to change Gallery in Impressie page , just go to the Page Tab , and fill it with gallery
    * Upload Menukaart : 
        For PDF file (Menu) that can show on top of page , and visitors can download it
    * Overons Page Setting :
        * Sub Menu List :
            Field for list of Menu Category on "Over Ons Page"
        * Selling Points :
            Field for customize "selling points" section in Over Ons page ( above the footer )
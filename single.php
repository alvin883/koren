<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Korenbest
 */

get_header();

if(have_posts()) :
    while(have_posts()) : the_post(); ?>

    <div id="content">
        <div id="single">
            <div class="sequence">
                <div class="layer-bg">
                </div>

                <div class="section header image-parallax" <?php if( has_post_thumbnail() ){
                        echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                    } ?>>
                    <div class="container">
                        <div class="card green">
                            <div class="card-body">
                                <h1 class="card-title"> <?php the_title(); ?> </h1>
                                <div class="card-title-data">
                                    <?php the_time('F jS, Y g:i a');?> | Posted by <?php the_author();?>
                                </div>
                            </div><!-- .card-body -->
                        </div><!-- .card -->
                    </div><!-- .container -->
                </div><!-- .section -->

                <div class="section content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-md-10 col-12 mx-auto" id="content-wp">
                                <?php the_content();?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-10 col-12 mx-auto" id="jssocials">
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- .sequence -->
        </div><!-- #overons -->
    </div><!-- #content-->

    <?php endwhile;
else:
    echo "Sorry, no post were found";
endif; ?>
<?php 
get_footer();
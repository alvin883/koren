
<div class="section section-3">
    <?php if(get_field('image_menukaart', 'option')) : ?>
        <?php $image = get_field('image_menukaart', 'option'); ?>
        <div class="background" style="background-image: url('<?php echo $image['url']; ?>')" >
        </div><!-- side background -->
    <?php endif; ?>

    <?php if (get_field('title_menukaart', 'option')): ?>
        <div class="container">
                <div class="card green">
                    <div class="card-body">
                        <h2 class="card-title"> <?php echo get_field('title_menukaart', 'option');  ?></h2>
                        <p class="card-text">
                            <?php the_field('content_menukaart', 'option'); ?>
                        </p>
                        <a href="<?php the_field('link_menukaart', 'option'); ?>" class="btn menu-btn">
                            Bekijk onze menukaart
                            <div><i class="fas fa-chevron-right"></i></div>
                        </a>
                    </div><!-- .card-body -->
                </div><!-- .card -->
        </div><!-- .container -->
    <?php endif; ?>
</div><!-- .section -->
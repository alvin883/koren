<!-- Its for post section -->

<div class="section section-2" id="section-2">
    <div class="content">
        <div class="container">
            <?php if (get_field('title_highlight', 'option')) : ?>
                <h2 class="title"><?php echo get_field('title_highlight', 'option'); ?></h2>
            <?php endif;
            if(get_field('content_highlight', 'option')) : ?>
                <div class="description">
                    <?php echo get_field('content_highlight', 'option'); ?> 
                </div>
            <?php endif; ?>
            <!-- Start List of cards -->
            <div class="row">
            <?php 
                if(have_rows('post_card', 'option')) :
                    while(have_rows('post_card', 'option')) : the_row();

                        $title = get_sub_field('title_post_card', 'option');
                        $content = get_sub_field('content_post_card', 'option');
                        $image = get_sub_field('image_post_card', 'option');
                        $link = get_sub_field('refer_page_link', 'option');
            ?>
                        <!-- Column -->
                        <div class="column col-md-6 col-lg-4">
                            <div class="card bordered">
                                <a href="<?php echo $link; ?>">
                                <div class="card-img-top" style="background-image: url('<?php echo $image['url']; ?>')"></div>
                                </a>
                                <div class="card-body have-action">
                                    <h3 class="card-title"><?php echo $title; ?></h3>
                                    <p class="card-text">
                                    <?php echo $content; ?>
                                    </p>
                                    <a href="<?php echo $link; ?>" class="btn">
                                        Lees meer 
                                        <div><i class="fas fa-chevron-right"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>        
                     <?php endwhile;
                endif; ?>

            </div> <!-- .list -->
        </div><!-- .container -->
    </div><!-- .content -->
</div><!-- .section -->
<div class="section section-1">
        <?php if( have_rows('slider', 'option') ): ?>
            <div class="slider">
                <?php
                 while ( have_rows('slider', 'option') ) : the_row(); 
                    
                    $title = get_sub_field('slider_title', 'option');
                    $content = get_sub_field('slider_content', 'option');
                    $btn_link = get_sub_field('button_link', 'option');
                    $btn_text = get_sub_field('button_text', 'option');
                    $img = get_sub_field('slider_image', 'option');

                ?>

                    <div class='slider-item middle' style='background-image: url(<?php echo $img["url"]; ?>)'>
                        <div class="container">
                            <h3 class="title"><?php echo $title; ?></h3>
                            <div class="content"><?php echo $content; ?></div>
                            <div class="actions">
                                <a href="<?php echo $btn_link; ?>" class="btn outline"><?php echo $btn_text; ?></a>
                                <button class="btn round next-section"><i class="fas fa-chevron-down"></i></button>
                            </div>
                        </div>
                    </div>

                <?php endwhile; // loop through the row ?>
            </div><!-- .slider -->

        <?php else : ?>
            <div class="middle">
                <div class="wrap">
                    <h2 class="title"><?php $title ?></h2>
                    <div class="content">
                       <?php $content ?>
                    </div>
                    <div class="actions">
                        <button class="btn outline">Meer over ons</button>
                        <button class="btn round next-section"><a href="#section-2"><i class="fas fa-chevron-down"></i></a></button>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    <div class="bottom">
        <div class="container">
            <?php korenbest_menu('middle'); ?>
        </div>
    </div>
    
</div>